-- Answers to Exercise 2 here
CREATE TABLE programmer (
  username varchar (20),
  first_name varchar (50),
  last_name varchar (50),
  email varchar (50)
);

INSERT INTO programmer VALUES ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com');
INSERT INTO programmer VALUES (null, 'Peter', null, null);
INSERT INTO programmer VALUES (null, 'Pete', null, null);
INSERT INTO programmer VALUES (null, null, 'Peterson', null);

select * from programmer;

DELETE FROM programmer
WHERE last_name = 'Peterson';


INSERT INTO programmer VALUES ('programmer1', null, null , null );
