-- Answers to Exercise 3 here


CREATE TABLE customers (
  name varchar(50),
  gender varchar (6),
  year_born int (4),
  joined int (4),
  num_hires int (6)

);



INSERT INTO customers VALUES ('Peter Jackson', 'male', 1961, 1997, 17000);

INSERT INTO customers VALUES ('Jane Campion', 'female', 1954, 1980, 30000);

INSERT INTO customers VALUES ('Roger Donaldson', 'male', 1945, 1980, 12000);

INSERT INTO customers VALUES ('Temuera Morrison', 'male', 1960, 1995, 15500);

INSERT INTO customers VALUES ('Russell Crowe', 'male', 1964, 1990, 10000);

INSERT INTO customers VALUES ('Lucy Lawless', 'female', 1968, 1995, 5000);

INSERT INTO customers VALUES ('Michael Hurst', 'male', 1957, 2000, 15000);

INSERT INTO customers VALUES ('Andrew Niccol', 'male', 1964, 1997, 3500);

INSERT INTO customers VALUES ('Kiri Te Kanawa', 'female', 1944, 1997, 500);

INSERT INTO customers VALUES ('Lorde', 'female', 1996, 2010, 1000);

INSERT INTO customers VALUES ('Scribe', 'male', 1979, 2000, 5000);

INSERT INTO customers VALUES ('Kimbra', 'female', 1990, 2005, 7000);

INSERT INTO customers VALUES ('Neil Finn', 'male', 1958, 1985, 6000);

INSERT INTO customers VALUES ('Anika Moa', 'female', 1980, 2000, 700);

INSERT INTO customers VALUES ('Bic Runga', 'female', 1976, 1995, 5000);

INSERT INTO customers VALUES ('Ernest Rutherford', 'male', 1871, 1930, 4200);

INSERT INTO customers VALUES ('Kate Sheppard', 'female', 1847, 1930, 1000);

INSERT INTO customers VALUES ('Apirana Turupa Ngata', 'male', 1874, 1920, 3500);

INSERT INTO customers VALUES ('Edmund Hillary', 'male', 1919, 1955, 10000);

INSERT INTO customers VALUES ('Katherine Mansfield', 'female', 1888, 1920, 2000);

INSERT INTO customers VALUES ('Margaret Mahy', 'female', 1936, 1985, 5000);

INSERT INTO customers VALUES ('John Key', 'male', 1961, 1990, 20000);

INSERT INTO customers VALUES ('Sonny Bill Williams', 'male', 1985, 1995, 15000);

INSERT INTO customers VALUES ('Dan Carter', 'male', 1982, 1990, 20000);

INSERT INTO customers VALUES ('Bernice Mene', 'female', 1975, 1990, 30000);

