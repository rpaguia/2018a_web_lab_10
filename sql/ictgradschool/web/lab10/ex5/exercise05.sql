-- Answers to Exercise 5 here

CREATE TABLE programmerList (
  username varchar (20),
  first_name varchar (50),
  last_name varchar (50),
  email varchar (50)
);

DROP TABLE programmerList;


CREATE TABLE programmerList (
  username varchar (20),
  first_name varchar (50),
  last_name varchar (50),
  email varchar (50),
  PRIMARY KEY (username)
);

SELECT * FROM programmerList;

INSERT INTO programmer VALUES ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com');
INSERT INTO programmer VALUES (null, 'Peter', null, null);
INSERT INTO programmer VALUES (null, 'Pete', null, null);
INSERT INTO programmer VALUES (null, null, 'Peterson', null);