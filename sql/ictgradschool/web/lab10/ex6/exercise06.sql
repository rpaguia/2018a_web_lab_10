-- Answers to Exercise 6 here



CREATE TABLE customersd (
  customer_no int not null,
  name varchar(50),
  gender varchar (6),
  year_born int (4),
  joined int (4),
  num_hires int (6),
  primary key (customer_no)

);

INSERT INTO customersd VALUES (1, 'Ronnie', 'male', 1995, 2010, 103);
INSERT INTO customersd VALUES (2, 'Alex', 'male', 1982, 2015, 10001);
INSERT INTO customersd VALUES (3, 'Mark', 'male', 1977, 2007, 8172);

select * from customersd

CREATE TABLE customersVideos (
  customer_no int,
  customerName varchar(50),
  movieID int NOT NULL,
  title varchar(50),
  director varchar(50),
  charge_rate float(2),
  PRIMARY KEY (movieID),
  FOREIGN KEY (customer_no) REFERENCES customersd(customer_no)
);

drop table customersb

drop table customersc


INSERT INTO customersVideos VALUES (1, 'Ronnie', '1001', 'Jumanji', 'Spielberg', 103.03);
INSERT INTO customersVideos VALUES (1, 'Ronnie', '1002', 'Playstore', 'Spielberg', 104.13);
INSERT INTO customersVideos VALUES (1, 'Ronnie', '1003', 'Bourne', 'Spielberg', 94.03);

select * FROM customersVideos