-- Answers to Exercise 8 here


select * from customers;

-- delete a row(s)
delete from customers
where name = 'Peter Jackson';


# -- delete column
# alter gender from customers;
# drop column gender;


-- delete table, then reinstating the file.
drop table customers;

# create table customers(
#
# )


-- change a value that is a string
update customers
set name = 'Ronnie Paguia'
where name = 'Peter Jackson';



-- change a value that is a numeric value
update customers
set year_born = 1977
where year_born = 1961 and name = 'Ronnie Paguia';



-- change a value that is a primary key

select *
from websiteArticles;

update websiteArticles
set id = 5
where id = 1;







