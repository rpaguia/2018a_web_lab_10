-- Answers to Exercise 9 here

-- All information of members in the video store
select * from customersVideos;


-- everything about video store less video hires
select name, gender, year_born, joined
from customers;


-- all title to the articles written
select title
from websiteArticles;


-- directors of the videos
select distinct director
from customersVideos;


-- video titles that for less than $2 or less a week.
select * from customersVideos;

select charge_rate
from customersVideos
where charge_rate < 100.00;


-- sorted list of all the usernames that have been registered
select username
from programmer
order by username ASC ;

-- sorted list where fname starts with 'Pete'
select *
from programmer
where last_name like 'Pete%';


-- usernames where user;s first name or last name starts with "Pete"
select *
from programmer
where last_name like 'Pete%' or first_name like 'Pete%';
